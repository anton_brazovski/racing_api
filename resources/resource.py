from flask_restful import Resource
from os import environ as env


class BaseResource(Resource):
    def get(self):
        return {'hello': 'world', 'test': env['TEST_MSG']}
