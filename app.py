from flask import Flask
from flask_restful import Resource, Api
from resources.resource import BaseResource

app = Flask(__name__)
app.config['BUNDLE_ERRORS'] = True

api = Api(app)



api.add_resource(BaseResource, '/')
# api.add_resource(User, '/user', '/<int:user_id>')
# api.add_resource(Car, '/car')
# api.add_resource(CarBuying, '/car-buying')


if __name__ == '__main__':
    app.run(debug=True)
